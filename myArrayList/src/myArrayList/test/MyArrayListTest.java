package myArrayList.test;

import java.util.Arrays;

import myArrayList.MyArrayList;
import myArrayList.util.Results;

public class MyArrayListTest {

	public void testMe(MyArrayList myArrayList, Results results) {

		myArrayList.clear();
		testcase1(myArrayList, results); // out of range values
											// should not be
											// inserted into the array

		myArrayList.clear();
		
		testcase2(myArrayList, results); // in case of duplicate values,
											// the indexOf()
											// method should
											// return the index
											// of first
											// occurrence of the value 
										

		myArrayList.clear();
		testcase3(myArrayList, results); // insert 99(99
											// to 0) elements and check array resizing 
											// and order of elements
											
											

		myArrayList.clear();
		testcase4(myArrayList, results); // ensure that the
											// overridden
											// toString
											// method displays
											// the contents of
											// the arraylist in
											// user readable
											// format.

		myArrayList.clear();
		testcase5(myArrayList, results); // ensure that the size
											// of arraylist is
											// reduced by 4
											// after
											// removeValue()
											// is passed an
											// element that has
											// 4
											// Occurrences in
											// the
											// list.

		myArrayList.clear();
		testcase6(myArrayList, results); // remove 3 elements
											// from arraylist
											// and
											// ensure that
											// elements are in
											// ascending order
											// after removal.

		myArrayList.clear();
		testcase7(myArrayList, results); // on clear(), ensure
											// that the size of
											// arraylist is 0
											// and toString
											// prints an empty
											// string on
											// console.

		myArrayList.clear();
		testcase8(myArrayList, results); // check size and sum
											// of an empty
											// arraylist.

		myArrayList.clear();
		testcase9(myArrayList, results); // insert the same
											// element in
											// arraylist
											// multiple times
											// and check the
											// size
											// of arraylist.

		myArrayList.clear();
		testcase10(myArrayList, results); // ensure that the
											// first element
											// Added
											// to empty
											// ArrayList is in
											// cell
											// zero

	}

	// 1. out of range values should not be inserted into the array.
	private void testcase1(MyArrayList myArrayList, Results results) {

		int[] expected = new int[2];
		String testName = "Test 1: Out of range values should not be inserted into the array: ";
		expected[0] = 10000;
		expected[1] = 0;
		Arrays.sort(expected);
		String expected_result = Arrays.toString(expected).replaceAll(",", "");
		expected_result = expected_result.replaceAll("\\[", "");
		expected_result = expected_result.replaceAll("\\]", "");
		expected_result = expected_result + " ";
		
		String actual_result = "";
		myArrayList.insertSorted(10000);
		myArrayList.insertSorted(10001);
		myArrayList.insertSorted(0);
		myArrayList.insertSorted(-1);

		actual_result = myArrayList.toString();

		String result_string;
		if (expected_result.equals(actual_result)) {
			result_string = testName + "Passed";
			

		} else {
			result_string = testName + "failed." + " Actual Result: " + actual_result + ". Expected Result: "
					+ expected_result;
		}

		results.storeNewResult(result_string);

	}

	// for duplicate values the indexOf() method should return the index of
	// first occurrence of the value.
	private void testcase2(MyArrayList myArrayList, Results results) {

		int[] expected = new int[1];
		String testName = "Test 2: For duplicate values the indexOf() method should return the index of first occurance of the value: ";
		expected[0] = 0;
		Arrays.sort(expected);
		String expected_result = Arrays.toString(expected).replaceAll(",", "");
		expected_result = expected_result.replaceAll("\\[", "");
		expected_result = expected_result.replaceAll("\\]", "");
		String actual_result = "";
		myArrayList.insertSorted(3);
		myArrayList.insertSorted(3);
		myArrayList.insertSorted(3);
		myArrayList.insertSorted(3);
		myArrayList.insertSorted(3);
		
		//System.out.println(myArrayList.toString());
		//System.out.println(myArrayList.indexOf(3));
		actual_result = String.valueOf(myArrayList.indexOf(3));

		String result_string;
		if (expected_result.equals(actual_result)) {
			result_string = testName + "Passed";
			

		} else {
			result_string = testName + "failed." + " Actual Result: " + actual_result + ". Expected Result: "
					+ expected_result;
		}

		results.storeNewResult(result_string);

	}

	// insert 100(99 to 0) elements into the arraylist and ensure correct
	// order of inserted elements.
	private void testcase3(MyArrayList myArrayList, Results results) {

		int[] expected = new int[100];
		String testName = "Test 3: Insert 100(99 to 0) elements into the arraylist and test array resizing and order of elements. ";

		int i, j;
		j = 0;
		for (i = 99; i >= 0; i--) {
			expected[j] = i;
			j++;
		}

		Arrays.sort(expected);
		String expected_result = Arrays.toString(expected).replaceAll(",", "");
		expected_result = expected_result.replaceAll("\\[", "");
		expected_result = expected_result.replaceAll("\\]", "");
		expected_result = expected_result + " ";
		
		String actual_result = "";

		for (i = 99; i >= 0; i--) {
			myArrayList.insertSorted(i);
		}
		actual_result = myArrayList.toString();

		String result_string;
		if (expected_result.equals(actual_result)) {
			result_string = testName + "Passed";
			

		} else {
			result_string = testName + "failed." + " Actual Result:"+ actual_result +". Expected Result: "
					+ expected_result;
		}

		results.storeNewResult(result_string);

	}

	// ensure that the overridden toString method displays the contents of the
	// arraylist in user readable format.
	private void testcase4(MyArrayList myArrayList, Results results) {

		int[] expected = new int[5];
		String testName = "Test 4: Ensure that the overridden toString method displays the contents of the arraylist in user readable format: ";
		expected[0] = 1;
		expected[1] = 2;
		expected[2] = 3;
		expected[3] = 4;
		expected[4] = 5;
		Arrays.sort(expected);
		String expected_result = Arrays.toString(expected).replaceAll(",", "");
		expected_result = expected_result.replaceAll("\\[", "");
		expected_result = expected_result.replaceAll("\\]", "");
		expected_result = expected_result + " ";
		
		String actual_result = "";
		int i;
		for (i = 1; i < 6; i++) {
			myArrayList.insertSorted(i);

		}
		actual_result = myArrayList.toString();

		String result_string;
		if (expected_result.equals(actual_result)) {
			result_string = testName + "Passed";
			

		} else {
			result_string = testName + "failed." + " Actual Result: " + actual_result + ". Expected Result: "
					+ expected_result;
		}

		results.storeNewResult(result_string);

	}

	// ensure that the size of arraylist is reduced by 4 after removeValue() is
	// passed an element that has 4 occurances in the list.
	private void testcase5(MyArrayList myArrayList, Results results) {
		int[] expected = new int[1];
		expected[0] = 2;
		String testName = "Test 5: Ensure that the size of arraylist is reduced by 4 after removeValue() is passed an element that has 4 occurances in the list: ";
		Arrays.sort(expected);
		String expected_result = Arrays.toString(expected).replaceAll(",", "");
		expected_result = expected_result.replaceAll("\\[", "");
		expected_result = expected_result.replaceAll("\\]", "");
		String actual_result = "";

		myArrayList.insertSorted(1);
		myArrayList.insertSorted(2);
		myArrayList.insertSorted(3);
		myArrayList.insertSorted(3);
		myArrayList.insertSorted(3);
		myArrayList.insertSorted(3);

		myArrayList.removeValue(3);
		actual_result = String.valueOf(myArrayList.size()).trim();

		String result_string;
		if (expected_result.equals(actual_result)) {
			result_string = testName + "Passed";

		} else {
			result_string = testName + "failed." + " Actual Result: " + actual_result + ". Expected Result: "
					+ expected_result;
		}

		results.storeNewResult(result_string);

	}

	// remove 3 elements from arraylist and ensure that elements are in
	// ascending order after removal.
	private void testcase6(MyArrayList myArrayList, Results results) {
		int[] expected = new int[3];
		expected[0] = 2;
		expected[1] = 4;
		expected[2] = 6;

		String testName = "Test 6: Remove 3 elements from arraylist and ensure that elements are in ascending order after removal: ";
		Arrays.sort(expected);
		String expected_result = Arrays.toString(expected).replaceAll(",", "");
		expected_result = expected_result.replaceAll("\\[", "");
		expected_result = expected_result.replaceAll("\\]", "");
		expected_result = expected_result + " ";
		
		String actual_result = "";

		myArrayList.insertSorted(1);
		myArrayList.insertSorted(2);
		myArrayList.insertSorted(3);
		myArrayList.insertSorted(4);
		myArrayList.insertSorted(5);
		myArrayList.insertSorted(6);

		myArrayList.removeValue(1);
		myArrayList.removeValue(3);
		myArrayList.removeValue(5);

		actual_result = myArrayList.toString();

		String result_string;
		if (expected_result.equals(actual_result)) {
			result_string = testName + "Passed";

		} else {
			result_string = testName + "failed." + " Actual Result: " + actual_result + ". Expected Result: "
					+ expected_result;
		}

		results.storeNewResult(result_string);
	}

	// on clear(), ensure that the size of arraylist is 0 and toString prints an
	// empty string on console.
	private void testcase7(MyArrayList myArrayList, Results results) {
		int[] expected = new int[1];
		expected[0] = 0;
		String testName = "Test 7: On clear(), ensure that the size of arraylist is 0 and toString prints an empty string on console: ";
		Arrays.sort(expected);
		String expected_result = Arrays.toString(expected).replaceAll(",", "");
		expected_result = expected_result.replaceAll("\\[", "");
		expected_result = expected_result.replaceAll("\\]", "");
		expected_result = expected_result + "";
		String actual_result = "";
		actual_result = String.valueOf(myArrayList.size()).trim() + myArrayList.toString();
		String result_string;
		if (expected_result.equals(actual_result)) {
			result_string = testName + "Passed";

		} else {
			result_string = testName + "failed." + " Actual Result: " + actual_result + ". Expected Result: "
					+ expected_result;
		}

		results.storeNewResult(result_string);

	}

	// check size and sum of an empty arraylist.
	private void testcase8(MyArrayList myArrayList, Results results) {
		int[] expected = new int[2];
		expected[0] = 0;
		expected[1] = 0;
		String testName = "Test 8: Check size and sum of an empty arraylist: ";
		Arrays.sort(expected);
		String expected_result = Arrays.toString(expected).replaceAll(",", "");
		expected_result = expected_result.replaceAll("\\[", "");
		expected_result = expected_result.replaceAll("\\]", "");
		expected_result = expected_result + "";
		String actual_result = "";
		actual_result = String.valueOf(myArrayList.size()).trim() + " " + String.valueOf(myArrayList.sum()).trim();

		String result_string;
		if (expected_result.equals(actual_result)) {
			result_string = testName + "Passed";

		} else {
			result_string = testName + "failed." + " Actual Result: " + actual_result + ". Expected Result: "
					+ expected_result;
		}

		results.storeNewResult(result_string);

	}

	// insert the same element in arraylist multiple times and check the size of
	// arraylist.
	private void testcase9(MyArrayList myArrayList, Results results) {
		int[] expected = new int[1];
		expected[0] = 50;
		String testName = "Test 9: Insert the same element in arraylist multiple times and check the size of arraylist: ";
		Arrays.sort(expected);
		String expected_result = Arrays.toString(expected).replaceAll(",", "");
		expected_result = expected_result.replaceAll("\\[", "");
		expected_result = expected_result.replaceAll("\\]", "");
		String actual_result = "";

		int i;
		for (i = 0; i < 50; i++) {
			myArrayList.insertSorted(13);
		}

		actual_result = String.valueOf(myArrayList.size()).trim();

		String result_string;
		if (expected_result.equals(actual_result)) {
			result_string = testName + "Passed";

		} else {
			result_string = testName + "failed." + " Actual Result: " + actual_result + ". Expected Result: "
					+ expected_result;
		}

		results.storeNewResult(result_string);

	}

	// ensure that the first element added to empty ArrayList is in cell zero
	private void testcase10(MyArrayList myArrayList, Results results) {
		int[] expected = new int[1];
		expected[0] = 0;
		String testName = "Test 10: Ensure that the first element added to empty ArrayList is in cell zero: ";
		Arrays.sort(expected);
		String expected_result = Arrays.toString(expected).replaceAll(",", "");
		expected_result = expected_result.replaceAll("\\[", "");
		expected_result = expected_result.replaceAll("\\]", "");
		String actual_result = "";
		myArrayList.insertSorted(5);

		actual_result = String.valueOf(myArrayList.indexOf(5)).trim();
		String result_string;
		if (expected_result.equals(actual_result)) {
			result_string = testName + "Passed";

		} else {
			result_string = testName + "failed." + " Actual Result: " + actual_result + ". Expected Result: "
					+ expected_result;
		}

		results.storeNewResult(result_string);

	}

}