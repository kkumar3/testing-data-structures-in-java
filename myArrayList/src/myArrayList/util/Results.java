package myArrayList.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class Results implements FileDisplayInterface, StdoutDisplayInterface{

//string array to store test results	
	private String[] storedStrings = new String[11];
	int i=0;
	
//setter function	
	public void setStoredStrings(String[] s){
		for(i=0; i<s.length; i++){
			storedStrings[i] = s[i];
		}
	} 
//getter function	
	public int getStoredStrings(){
		for(int i=0; i<storedStrings.length; i++){
			System.out.println(storedStrings[i]+"\n");
		}
		return 0;
	}
//implementation of interface methods	
	public void writeToFile(File f,String s){
		try {
			
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "utf-8")); 
			writer.write(s);
		    
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void writeToStdout( String s){
		System.out.println("The string is"+s+"\n");
	}

//function to store test results in string array.	
	public void storeNewResult(String s1){
		storedStrings[i] = s1;
		i++;
	}
	
	
}
