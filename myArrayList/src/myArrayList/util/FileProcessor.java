package myArrayList.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileProcessor {
//File processor class which implements readLine function
//which reads one line from input file and returns the same
//on consecutive calls to readline, line number read will be incremented by 1	
	static int linecount = 0;
	protected String readLine(String ip){
		try{
			
			File f = new File(ip);
		
			BufferedReader b = new BufferedReader(new FileReader(f));
			
			
			for(int i=0; i<linecount; i++){
				
			
				if(b.readLine()!= null) {
			
					linecount++;
				}
			}
			
			return b.readLine();
			
		} catch (IOException e) {
            e.printStackTrace();
        }
		return null; 
	}

}
