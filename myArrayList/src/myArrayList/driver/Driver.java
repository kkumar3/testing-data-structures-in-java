package myArrayList.driver;

import java.io.IOException;
import java.io.PrintWriter;
import myArrayList.MyArrayList;
import myArrayList.test.MyArrayListTest;
import myArrayList.util.Results;



public class Driver 
{

//main function
	public static void main(String[] args) throws IOException 
	{
//object instantiation 		
		MyArrayList d1 = new MyArrayList();
		Results r1 = new Results();
		MyArrayListTest t1 = new MyArrayListTest();
		
//call to test method in MyArrayListTest class 
		t1.testMe(d1, r1); 

//reading input values from input file
		try {
			d1.readValues(args[0]);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
//writing output to output file if it exists else create output file if it doesn't exists.		
		try{
		    PrintWriter writer = new PrintWriter(args[1], "UTF-8");
		    writer.println("The sum of all the values in the array list is: " + d1.sum());
		    
		    writer.close();
		} catch (IOException e) {
		   e.printStackTrace();
		}
		
		
//printing test results to Console using method from Result class		
		System.out.println("The test result is");
		r1.getStoredStrings();
	}
	
}
