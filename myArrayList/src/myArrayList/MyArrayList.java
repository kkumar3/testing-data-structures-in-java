
package myArrayList;
	
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;




public class MyArrayList 
{
//private data variables
	private int myArray[] = new int[50]; 
	private int gIndx = 0;		//index to keep track of size of array
	private int currSize = 50;	// current base size of array

//constructor	
	public MyArrayList(){		
		
		for(int i=0;i<myArray.length;i++){
			myArray[i] = -1;
		}
		
	}
//setter function	
	public void setMyArray(int[] arr){
		for(int i=0; i<arr.length; i++){
			myArray[i] = arr[i];
		}
	}

//getter function	
	public int[] getMyArray(){
		return myArray;
	}
	
//function to clear contents of array and reset values	
	public void clear(){
		for(int i=0; i<myArray.length; i++){
			myArray[i] = -1;
		}
		gIndx = 0;
	}

//function to extend the size of array	
	public int[] extendArray( int[] arr){ 
		
		int incArr[] = new int[(arr.length + 25)];
		for(int i=0; i<arr.length; i++){
			incArr[i] = arr[i];
		}
		currSize = incArr.length;
		return incArr;
	}
	
	
//function to insert element in array and then sort it.	
	public void insertSorted(int newValue){
		
		if(newValue < 10001 && newValue >= 0 ){	
			if((gIndx) < currSize){
			
				
				myArray[gIndx] = newValue;
				
				gIndx++;
				Arrays.sort(myArray,0,gIndx);
			}
			else{						//if array size extends, the array will be extended
										//before inserting a new element
				myArray = extendArray(myArray);
				myArray[gIndx] = newValue;
				
				gIndx++;
				Arrays.sort(myArray,0,gIndx);
			
			}
		}
		
	}	

//function to remove an element.	
	public void removeValue(int value){
		
		for(int i=0; i<myArray.length; i++){
			if(myArray[i] == value){
				for(int k=i; k<(myArray.length-1); k++){
					myArray[k] = myArray[k+1];
					
				}
				i--;
			}
		}
	}
	
//function to find index of an element in array	
	public int indexOf(int value){
		
		
		for(int i=0; i<myArray.length; i++){
			
			
			if(myArray[i] == value && myArray[i]!= -1){
				
				return i;
			}
		}
		return 0;
	}
	
	
//function to find the size of array	
	public int size(){
		int size=0;
		for(int i=0;i<myArray.length;i++){
			if(myArray[i]!= (-1)){
				size++;
			}
		}
		
		return size;
	}

//function to find the sum of array.	
	public int sum(){
		int sum=0;
		for(int i=0;i<myArray.length;i++){
			if(myArray[i] != -1){
				sum+=myArray[i];
		
			}
		}
		return sum;
	}

//Overridden toString function
	public String toString(){
	
		StringBuilder s = new StringBuilder();
		
		for(int i=0; i<myArray.length; i++){
			if(myArray[i] != (-1)){
				s.append(myArray[i]+" ");
			}	
		}
		
		return s.toString();
	}
	
//function to read elements from a file and insert them in array	
	public void readValues(String ipFile) throws IOException{
		
		int k = 0;	
		try{
			
			File f = new File(ipFile);
		
			BufferedReader b = new BufferedReader(new FileReader(f));
			
			String line = "";
			while ((line = b.readLine())!= null) {
			
				myArray[k] = Integer.parseInt(line);
				
				k++;
				
			}
			
		} catch (IOException e) {
            e.printStackTrace();
        } 
		
	}
		
}
